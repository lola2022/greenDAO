/*
  * Copyright (c) 2022 Huawei Device Co., Ltd.
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
    *
  * http://www.apache.org/licenses/LICENSE-2.0
    *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

import {Property} from '@ohos/greendao';
import {Note} from './Note'
import {NoteType} from './NoteType'
import {DaoSession} from '@ohos/greendao'
import {BaseDao} from '@ohos/greendao'
import {Query} from '@ohos/greendao'
import {QueryBuilder} from '@ohos/greendao'
import {qury} from '@ohos/greendao'
import {TableAction} from '@ohos/greendao'
import {OnTableChangedListener} from '@ohos/greendao'

var defaultTmp; // = globalThis.exports.default;
var daoSess: DaoSession;
var noteDaos: BaseDao<Note, number>;
var notesQ: Query<Note>;


@Entry
@Component
struct Index {
  @State arr: Array<Note> = new Array<Note>();
  @State editFlag: boolean = false
  @State noteText: string= ''
  @State daoSession: DaoSession = daoSess;
  @State noteDao: BaseDao<Note, number> = noteDaos;
  @State notesQuery: Query<Note> = notesQ ;

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Flex() {
        // @ts-ignore
        TextInput({ placeholder: 'Enter new note', text: this.historyInput })
          .type(InputType.Normal)
          .placeholderColor(Color.Gray)
          .placeholderFont({ size: 20, weight: 2 })
          .enterKeyType(EnterKeyType.Search)
          .caretColor(Color.Green)
          .layoutWeight(3)
          .height(45)
          .borderRadius('0px')
          .backgroundColor(Color.White)
          .onChange((value: string) => {
            this.noteText = value
          })
        Button('ADD')
          .fontSize(13)
          .fontWeight(FontWeight.Bold)
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            this.addNote()
          })
        Button('Query')
          .fontSize(13)
          .fontWeight(FontWeight.Bold)
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            this.query()
          })
      }
      .margin({ top:12 })

      Flex() {
        Button('Delete bbb and Query')
          .height(45)
          .fontSize(12)
          .padding({ top:8, left: 8 })
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            this.deleteNotes()
          })
        Button('Query aaa bbb ccc')
          .height(45)
          .fontSize(12)
          .padding({ top:8 })
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            this.selectWhere()
          })
      }
      .margin({ top:12 })
      Flex() {
        Button('Load(id 2的缓存)')
          .height(45)
          .fontSize(12)
          .padding({ top:1, left: 8 })
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            this.loadId2();
          })
        Button('save(没主键时新增)')
          .height(45)
          .fontSize(12)
          .padding({ top:1 })
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            let note= new Note();
            note.setText("Note_a");
            noteDaos.save(note)
          })
      }.margin({ top:5 })

      Flex() {
        Button('flexQuery')
          .height(45)
          .fontSize(12)
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            new qury().from(Note)
              .query(Note)
              .then((data) => {
                if(data)
                  this.arr = data;
                else
                  this.arr = [];
              })
          })
        Button('query id 2')
          .height(45)
          .fontSize(12)
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            new qury().from(Note)
              .eq("ID", 2)
              .querySingle(Note)
              .then((data) => {
                if(data)
                  this.arr = data;
                else
                  this.arr = [];
              })
          })
        Button('query aaa')
          .height(45)
          .fontSize(12)
          .layoutWeight(1)
          .onClick((event: ClickEvent) => {
            new qury().from(Note)
              .eq("TEXT", "aaa")
              .querySingle(Note)
              .then((data) => {
                if(data)
                  this.arr = data;
                else
                  this.arr = [];
              })
          })
      }.padding({ top: 4, left: 10})


      List({ space: 20, initialIndex: 0 }) {
        ForEach(this.arr, (item) => {
          ListItem() {
            Flex({ direction: FlexDirection.Row,wrap:FlexWrap.Wrap }) {
              Text('' + item.text)
                .fontSize(13)
              Button("删除").onClick(() => {
                noteDaos.deleteByKey(item.id)
              })
              Button("修改").onClick(() => {
                item.setText(this.noteText)
                noteDaos.update(item)
              })
              Button('save')
                .onClick(() => {
                  item.setText(this.noteText)
                  noteDaos.save(item);
                })
              Button('refresh')
                .onClick(() => {
                  item.setText(this.noteText)
                  noteDaos.refresh(item);
                  var that = this
                  setTimeout(function () {
                    that.updateNotes();
                  }, 1000)
                })
              Button('refresh前修改')
                .onClick(() => {
                  this.refreshUpdate(item.id)
                })
            }.width('100%')

          }
        }, item => item.id)
      }
      .listDirection(Axis.Vertical) // 排列方向
      .divider({ strokeWidth: 2, color: 0xFFFFFF, startMargin: 20, endMargin: 20 }) // 每行之间的分界线
      .edgeEffect(EdgeEffect.None) // 滑动到边缘无效果
      .chainAnimation(false) // 联动特效关闭
      .onScrollIndex((firstIndex: number, lastIndex: number) => {
      })
      .editMode(this.editFlag)
      .onItemDelete((index: number) => {
        this.arr.splice(index, 1)
        this.editFlag = false
        return true
      })
      .width('90%')
    }
    .width('100%')
    .height('100%')
  }

   refreshUpdate(id:number){
    var that=this;
    let data=new Array<Note>();
    for(var i=0;i<this.arr.length;i++){
      if(this.arr[i].id==id){
        this.arr[i].text=this.arr[i].text+"--2"
      }
      data[i]=this.arr[i]
    }
    that.arr=data;
  }

   addNote() {
    try {
      let date = new Date()
      let comment = "Added on " + date.toLocaleString();

      let note = new Note();
      note.setText(this.noteText);
      note.setComment(comment);
      note.setDate(new Date());
      note.setType(NoteType[NoteType.TEXT]);
      noteDaos.insert(note);
    } catch (e) {
      console.info("resultSet addNote err:" + e)
    }
  }

   async updateNotes() {
    this.arr = await this.notesQuery.list();
  }

   async query() {

    let properties = defaultTmp.data[Note.name];
    let query = this.noteDao.queryBuilder().orderAsc(properties['text']).buildCursor();
    // @ts-ignore
    let cursor: ResultSet = await query.query();

    let a = this.noteDao.convertCursor2Entity(cursor);
    if(!a) a = [];
    this.arr = a;
  }

   async deleteNotes() {
    try {
      let properties = defaultTmp.data[Note.name];
      let deleteQuery = this.noteDao.queryBuilder().where(properties['text'].eq("bbb"))
        .buildDelete();
      deleteQuery.executeDeleteWithoutDetachingEntities()

    } catch (e) {
      console.info("deleteNotes--------" + e)
    }
  }

   async selectWhere() {
    try {
      let queryBuilder: QueryBuilder<Note> = this.noteDao.queryBuilder();
      let properties1 = defaultTmp.data[Note.name];
      let properties: Property[] = BaseDao.generatorProperties(Note);
      queryBuilder.whereOr(BaseDao.properties['text'].eq("aaa"), BaseDao.properties['text'].eq("bbb"), BaseDao.properties['text'].eq("ccc"));
      let aa: Array<Note> = await queryBuilder.list();
      if(!aa) aa = [];
      this.arr = aa;
    } catch (e) {
      console.info("index selectWhere----" + e)
    }
  }


   private aboutToAppear() {
    this.getAppData();
  }

   getAppData() {
    let that = this;
    setTimeout(async function () {
      defaultTmp = globalThis.exports.default;
      if (defaultTmp != undefined) {
        daoSess = await defaultTmp.data.daoSession;
        that.daoSession = daoSess;

        noteDaos = that.daoSession.getBaseDao(Note);
        that.noteDao = noteDaos;

        /*
         *添加监听
         */
        noteDaos.addTableChangedListener(that.tabListener())
        let properties: Property[] = BaseDao.generatorProperties(Note);
        that.notesQuery = that.noteDao.queryBuilder().orderAsc(BaseDao.properties['text']).build();
      }
    }, 1500);
  }

   onBackPress(){
    /**
     * 移除监听
     */
    noteDaos.removeTableChangedListener();

  }

   tabListener(): OnTableChangedListener<any>{
    let that = this;
    return {
      async onTableChanged(t: any, err, action: TableAction) {
        console.info('--------操作 00--------')
        if(err){
          console.info('--------操作 err--------'+err);
        }else
        if (action == TableAction.INSERT) {
          console.info('--------插入--------')
          await that.updateNotes();
        } else if(action == TableAction.UPDATE){
          console.info('--------编辑--------')
          await that.updateNotes();
        } else if(action == TableAction.DELETE){
          console.info('--------删除--------')
          await that.updateNotes();
        } else if (action == TableAction.QUERY) {
          console.info('--------查询-------- any:'+JSON.stringify(t))
        }
        console.info('--------操作 11--------')
      }
    }
  }

   async loadId2(){
    let tmp = await noteDaos.load(2);
    if(tmp){
      let a = [tmp];
      this.arr = a;
    }else{
      this.arr = [];
    }

  }

}