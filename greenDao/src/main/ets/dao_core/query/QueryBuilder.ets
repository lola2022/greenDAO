/*
 * Copyright (C) 2011-2016 Markus Junginger, greenrobot (http://greenrobot.org)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {AbstractDao} from '../AbstractDao'
import {Property} from '../Property'
import {WhereCollector} from './WhereCollector'
import {WhereCondition} from './WhereCondition'
import {CursorQuery} from './CursorQuery'
import {DeleteQuery} from './DeleteQuery'
import {CountQuery} from './CountQuery'
import {Join} from './Join'
import {Query} from './Query'
import {PropertyCondition} from './PropertyCondition';
import {CloseableListIterator} from './CloseableListIterator'
import dataRdb from '@ohos.data.rdb'
import {SqlUtils} from '../internal/SqlUtils'

export class QueryBuilder <T> {

  /** Set to true to debug the SQL. */
  public LOG_SQL: boolean;

  /** Set to see the given values. */
  public LOG_VALUES: boolean;
  private whereCollector: WhereCollector<T>;
  private orderBuilder: String;
  private values: Array<Object>;
  private joins: Array<Join<T, any>>;
  private dao: AbstractDao<T, any>;
  private tablePrefix: string;
  private limits: number;
  private offsets: number;
  private isDistinct: boolean;
  //新增RdbPredicates
  // @ts-ignore
  private predicates: dataRdb.RdbPredicates;
  /** stored with a leading space */
  private stringOrderCollations: string;

  /** For internal use by greenDAO only. */
  public static internalCreate<T2>(dao: AbstractDao<T2, any>): QueryBuilder<T2>{
    return new QueryBuilder<T2>(dao);
  }

  protected constructor(dao: AbstractDao<T, any>, tablePrefix?: string) {
    this.dao = dao;
    this.predicates = new dataRdb.RdbPredicates(this.dao.getTablename())
    if (tablePrefix == null) {
      this.tablePrefix = "T";
    } else {
      this.tablePrefix = tablePrefix;
    }
    this.values = new Array<Object>();
    this.joins = new Array<Join<T, any>>();
    this.whereCollector = new WhereCollector<T>(dao, this.tablePrefix);
    this.stringOrderCollations = " COLLATE NOCASE";
  }

  private checkOrderBuilder() {
    if (this.orderBuilder == null) {
      this.orderBuilder = new String();
    } else if (this.orderBuilder.length > 0) {
      this.orderBuilder = this.orderBuilder.concat(",");
    }
  }

  /** Use a SELECT DISTINCT to avoid duplicate entities returned, e.g. when doing joins. */
  public distinct(): QueryBuilder<T>{
    this.isDistinct = true;
    this.predicates.distinct()
    return this;
  }

  /**
     * If using embedded SQLite, this enables localized ordering of strings
     * (see {@link #orderAsc(Property...)} and {@link #orderDesc(Property...)}). This uses "COLLATE LOCALIZED", which
     * is unavailable in SQLCipher (in that case, the ordering is unchanged).
     *
     * @see #stringOrderCollation
     */
  public preferLocalizedStringOrder(): QueryBuilder<T> {
    // SQLCipher 3.5.0+ does not understand "COLLATE LOCALIZED"
    //        if (dao.getDatabase().getRawDatabase() instanceof SQLiteDatabase) {
    this.stringOrderCollations = " COLLATE LOCALIZED";
    //        }
    return this;
  }

  /**
     * Customizes the ordering of strings used by {@link #orderAsc(Property...)} and {@link #orderDesc(Property...)}.
     * Default is "COLLATE NOCASE".
     *
     * @see #preferLocalizedStringOrder
     */
  public stringOrderCollation(stringOrderCollation: string): QueryBuilder<T>{
    this.stringOrderCollations = stringOrderCollation == null || stringOrderCollation.startsWith(" ") ?
      stringOrderCollation : " " + stringOrderCollation;
    return this;
  }

  /**
     * Adds the given conditions to the where clause using an logical AND. To create new conditions, use the properties
     * given in the generated dao classes.
     */
  public where(cond: WhereCondition, ...condMore: Array<WhereCondition>): QueryBuilder<T>{
    this.appendWhereClause(cond, ...condMore)
    return this;
  }

  public appendWhereClause(cond: WhereCondition, ...condMore: WhereCondition[]) {
    this.whereCase(cond);
    for (var index = 0; index < condMore.length; index++) {
      this.predicates.and()
      this.whereCase(condMore[index]);
    }
  }

  /**
     * Adds the given conditions to the where clause using an logical OR. To create new conditions, use the properties
     * given in the generated dao classes.
     */
  public whereOr(cond1: WhereCondition, cond2: WhereCondition, ...condMore: Array<WhereCondition>): QueryBuilder<T>{
    this.or(cond1, cond2, condMore)
    return this;
  }

  /**
     * Creates a WhereCondition by combining the given conditions using OR. The returned WhereCondition must be used
     * inside {@link #where(WhereCondition, WhereCondition...)} or
     * {@link #whereOr(WhereCondition, WhereCondition, WhereCondition...)}.
     */
  public or(cond1: WhereCondition, cond2: WhereCondition, condMore: Array<WhereCondition>): WhereCondition {
        this.predicates.beginWrap();
        this.whereCase(cond1);
        this.predicates.or()
        this.whereCase(cond2);
        for (var i = 0;i < condMore.length; i++) {
          this.predicates.or()
          this.whereCase(condMore[i]);
        }
        this.predicates.endWrap();
    return this.whereCollector.combineWhereConditions("OR", cond1, cond2, condMore);
  }

  /**
     * Creates a WhereCondition by combining the given conditions using AND. The returned WhereCondition must be used
     * inside {@link #where(WhereCondition, WhereCondition...)} or
     * {@link #whereOr(WhereCondition, WhereCondition, WhereCondition...)}.
     */
  public and(cond1: WhereCondition, cond2: WhereCondition, condMore: Array<WhereCondition>): WhereCondition{
        this.whereCase(cond1);
        this.predicates.and()
        this.whereCase(cond2);
        for (var i = 0;i < condMore.length; i++) {
          this.predicates.and()
          this.whereCase(condMore[i]);
        }
    return this.whereCollector.combineWhereConditions("AND", cond1, cond2, condMore);
  }

  public whereCase(whereCondition: WhereCondition) {
    if (whereCondition.constructor.name == "PropertyCondition") {
      let op: string = (<PropertyCondition> whereCondition).op;
      let property: Property = (<PropertyCondition> whereCondition).property;
      let value: any = (<PropertyCondition> whereCondition).value;
      let values: any[] = (<PropertyCondition> whereCondition).values;
      switch (op) {

        case "eq":
          this.predicates.equalTo(property.columnName, value)
          break;
        case "notEq":
          this.predicates.notEqualTo(property.columnName, value)
          break;
        case "like":
          this.predicates.like(property.columnName, value)
          break;
        case "between":
          this.predicates.between(property.columnName, values[0], values[1])
          break;
        case "in":
          this.predicates.in(property.columnName, values)
          break;
        case "notIn":
          this.predicates.notIn(property.columnName, values)
          break;
        case "gt":
          this.predicates.greaterThan(property.columnName, value)
          break;
        case "lt":
          this.predicates.lessThan(property.columnName, value)
          break;
        case "ge":
          this.predicates.greaterThanOrEqualTo(property.columnName, value)
          break;
        case "le":
          this.predicates.lessThanOrEqualTo(property.columnName, value)
          break;
        case "isNull":
          this.predicates.isNull(property.columnName)
          break;
        case "isNotNull":
          this.predicates.isNotNull(property.columnName)
          break;
      }
    }
  }
  /**
     * Expands the query to another entity type by using a JOIN. The primary key property of the primary entity for
     * this QueryBuilder is used to match the given destinationProperty.
     */
  /*  public join<J>(destinationEntityClass: J, destinationProperty: Property): Join<T, J>{
//    return join(dao.getPkProperty(), destinationEntityClass, destinationProperty);
    return null
  }*/

  /**
     * Expands the query to another entity type by using a JOIN. The given sourceProperty is used to match the primary
     * key property of the given destinationEntity.
     */
  /*
    public <J> Join<T, J> join(Property sourceProperty, Class<J> destinationEntityClass) {
        AbstractDao<J, ?> destinationDao = (AbstractDao<J, ?>) dao.getSession().getDao(destinationEntityClass);
        Property destinationProperty = destinationDao.getPkProperty();
        return addJoin(tablePrefix, sourceProperty, destinationDao, destinationProperty);
    }

    */
  /**
     * Expands the query to another entity type by using a JOIN. The given sourceProperty is used to match the given
     * destinationProperty of the given destinationEntity.
     */
  /*
    public <J> Join<T, J> join(Property sourceProperty, Class<J> destinationEntityClass, Property destinationProperty) {
        AbstractDao<J, ?> destinationDao = (AbstractDao<J, ?>) dao.getSession().getDao(destinationEntityClass);
        return addJoin(tablePrefix, sourceProperty, destinationDao, destinationProperty);
    }

    */
  /**
     * Expands the query to another entity type by using a JOIN. The given sourceJoin's property is used to match the
     * given destinationProperty of the given destinationEntity. Note that destination entity of the given join is used
     * as the source for the new join to add. In this way, it is possible to compose complex "join of joins" across
     * several entities if required.
     */
  /*
    public <J> Join<T, J> join(Join<?, T> sourceJoin, Property sourceProperty, Class<J> destinationEntityClass,
                               Property destinationProperty) {
        AbstractDao<J, ?> destinationDao = (AbstractDao<J, ?>) dao.getSession().getDao(destinationEntityClass);
        return addJoin(sourceJoin.tablePrefix, sourceProperty, destinationDao, destinationProperty);
    } */

  /*  private addJoin<J>(sourceTablePrefix: string, sourceProperty: Property, destinationDao: AbstractDao<J, any>,
                     destinationProperty: Property): Join<T, J>{
    let joinTablePrefix = "J" + (this.joins.length + 1);
    let join = new Join<T, J>(sourceTablePrefix, sourceProperty, destinationDao, destinationProperty,
      joinTablePrefix);
    this.joins.push(join);
    return join;
  }*/


  /** Adds the given properties to the ORDER BY section using ascending order. */
  public orderAsc(properties: Property ): QueryBuilder<T> {
    this.predicates.orderByAsc(properties.columnName)
    return this;
  }

  /** Adds the given properties to the ORDER BY section using descending order. */
  public orderDesc(properties: Property): QueryBuilder<T> {
    this.predicates.orderByDesc(properties.columnName)
    return this;
  }

  /** Adds the given properties to the ORDER BY section using the given custom order. */
  /*  public orderCustom(property: Property, customOrderForProperty: string): QueryBuilder<T> {
    this.checkOrderBuilder();
    this.append(this.orderBuilder, property).concat(' ');
    this.orderBuilder.concat(customOrderForProperty);
    return this;
  }*/

  /**
     * Adds the given raw SQL string to the ORDER BY section. Do not use this for standard properties: orderAsc and
     * orderDesc are preferred.
     */
  /* public orderRaw(rawOrder: string): QueryBuilder<T>{
    this.checkOrderBuilder();
    this.orderBuilder = this.orderBuilder.concat(rawOrder);
    return this;
  }*/

  /*protected append(builder: String, property: Property): String{
    this.whereCollector.checkProperty(property);
    builder = builder.concat(this.tablePrefix)
      .concat('.')
      .concat('\'')
      .concat(property.columnName.toString())
      .concat('\'');
    return builder;
  }*/

  /** Limits the number of results returned by queries. */
  public limit(limit: number): QueryBuilder<T> {
    this.limits = limit;
    this.predicates.limitAs(limit)
    return this;
  }

  /**
     * Sets the offset for query results in combination with {@link #limit(int)}. The first {@code offset} results are
     * skipped and the total number of results will be limited by {@code limit}. You cannot use offset without limit.
     */
  public offset(offset: number): QueryBuilder<T>{
    this.offsets = offset;
    this.predicates.offsetAs(offset)
    return this;
  }

  /**
     * Builds a reusable query object (Query objects can be executed more efficiently than creating a QueryBuilder for
     * each execution.
     */
  public build(): Query<T>{

    let builder: String = this.createSelectBuilder();
    let limitPosition: number = this.checkAddLimit(builder);
    let offsetPosition: number = this.checkAddOffset(builder);

    let sql: string = builder.toString();
    this.checkLog(sql);

    return Query.create(this.dao, this.predicates, this.values, limitPosition, offsetPosition);
  }

  /**
     * Builds a reusable query object for low level database.Cursor access.
     * (Query objects can be executed more efficiently than creating a QueryBuilder for each execution.
     */
  public buildCursor(): CursorQuery<T>{
    let builder: String = this.createSelectBuilder();
    let limitPosition: number = this.checkAddLimit(builder);
    let offsetPosition: number = this.checkAddOffset(builder);

    let sql: string = builder.toString();
    this.checkLog(sql);

    return CursorQuery.create(this.dao, this.predicates, this.values, limitPosition, offsetPosition);
  }

  private createSelectBuilder(): String {

    let select: String = SqlUtils.createSqlSelect(this.dao.getTablename(), this.tablePrefix, this.dao.getAllColumns(), this.isDistinct);
    let builder: String = new String(select);

    this.appendJoinsAndWheres(builder, this.tablePrefix);
    return builder;
  }

  private checkAddLimit(builder: String): number {

    let limitPosition = -1;
    if (this.limits != null) {
      builder.concat(" LIMIT ?");
      this.predicates.limitAs(this.limits)
      this.values.push(this.limits);
      limitPosition = this.values.length - 1;
    }
    return limitPosition;
  }

  private checkAddOffset(builder: String): number {
    let offsetPosition = -1;
    if (this.offsets != null) {
      if (this.limits == null) {
        throw new Error("Offset cannot be set without limit");
      }
      this.predicates.limitAs(this.offsets)
      builder.concat(" OFFSET ?");
      this.values.push(this.offsets);
      offsetPosition = this.values.length - 1;
    }
    return offsetPosition;
  }

  /**
     * Builds a reusable query object for deletion (Query objects can be executed more efficiently than creating a
     * QueryBuilder for each execution.
     */
  public buildDelete(): DeleteQuery<T>{
     let tablename:string = this.dao.getTablename();
        let baseSql:string = SqlUtils.createSqlDelete(tablename, null);
        return DeleteQuery.create(this.dao, this.predicates, this.values);
  }

  /**
     * Builds a reusable query object for counting rows (Query objects can be executed more efficiently than creating a
     * QueryBuilder for each execution.
     */
  public buildCount(): CountQuery<T>{


    /*String tablename = dao.getTablename();
        String baseSql = SqlUtils.createSqlSelectCountStar(tablename, tablePrefix);
        StringBuilder builder = new StringBuilder(baseSql);
        appendJoinsAndWheres(builder, tablePrefix);

        String sql = builder.toString();
        checkLog(sql);

        return CountQuery.create(dao, sql, values.toArray());*/
    return null
  }

  private checkLog(sql: string) {
    if (this.LOG_SQL) {
      console.debug("Built SQL for query: " + sql);
    }
    if (this.LOG_VALUES) {
      console.debug("Values for query: " + this.values);
    }
  }

  private appendJoinsAndWheres(builder: String, tablePrefixOrNull: string) {
    this.values = [];
    for (var i = 0;i < this.joins.length; i++) {
      builder = builder.concat(" JOIN ");
      builder = builder.concat('"').concat(this.joins[i].daoDestination.getTablename()).concat('"').concat(' ');
      builder = builder.concat(this.joins[i].tablePrefix).concat(" ON ");

      //SqlUtils.appendProperty(builder, this.joins[i].sourceTablePrefix, this.joins[i].joinPropertySource).append('=');
      //SqlUtils.appendProperty(builder, this.joins[i].tablePrefix, this.joins[i].joinPropertyDestination);
    }
    let whereAppended: boolean = this.whereCollector != null;
    if (whereAppended) {
      builder = builder.concat(" WHERE ");
      this.whereCollector.appendWhereClause(builder, tablePrefixOrNull, this.values);
    }
    for (var i = 0;i < this.joins.length; i++) {
      if (this.joins[i].whereCollector != null) {
        if (!whereAppended) {
          builder = builder.concat(" WHERE ");
          whereAppended = true;
        } else {
          builder = builder.concat(" AND ");
        }
        this.joins[i].whereCollector.appendWhereClause(builder, this.joins[i].tablePrefix, this.values);
      }
    }
  }

  /**
     * Shorthand for {@link QueryBuilder#build() build()}.{@link Query#list() list()}; see {@link Query#list()} for
     * details. To execute a query more than once, you should build the query and keep the {@link Query} object for
     * efficiency reasons.
     */
  public list(): Promise<Array<T>>{
    return this.build().list();
  }

  /**
     * Shorthand for {@link QueryBuilder#build() build()}.{@link Query#listLazy() listLazy()}; see
     * {@link Query#listLazy()} for details. To execute a query more than once, you should build the query and keep the
     * {@link Query} object for efficiency reasons.
     */
  /*
    public LazyList<T> listLazy() {
        return build().listLazy();
    }

    */
  /**
     * Shorthand for {@link QueryBuilder#build() build()}.{@link Query#listLazyUncached() listLazyUncached()}; see
     * {@link Query#listLazyUncached()} for details. To execute a query more than once, you should build the query and
     * keep the {@link Query} object for efficiency reasons.
     */
  /*
    public LazyList<T> listLazyUncached() {
        return build().listLazyUncached();
    }

    */
  /**
     * Shorthand for {@link QueryBuilder#build() build()}.{@link Query#listIterator() listIterator()}; see
     * {@link Query#listIterator()} for details. To execute a query more than once, you should build the query and keep
     * the {@link Query} object for efficiency reasons.
     */
  public listIterator(): CloseableListIterator<T> {
    return this.build().listIterator();
  }

  /**
     * Shorthand for {@link QueryBuilder#build() build()}.{@link Query#unique() unique()}; see {@link Query#unique()}
     * for details. To execute a query more than once, you should build the query and keep the {@link Query} object for
     * efficiency reasons.
     */
  public unique(): T {
    return this.build().unique();
  }

  /**
     * Shorthand for {@link QueryBuilder#build() build()}.{@link Query#uniqueOrThrow() uniqueOrThrow()}; see
     * {@link Query#uniqueOrThrow()} for details. To execute a query more than once, you should build the query and
     * keep
     * the {@link Query} object for efficiency reasons.
     */
  public uniqueOrThrow(): T{
    return this.build().uniqueOrThrow();
  }

  /**
     * Shorthand for {@link QueryBuilder#buildCount() buildCount()}.{@link CountQuery#count() count()}; see
     * {@link CountQuery#count()} for details. To execute a query more than once, you should build the query and keep
     * the {@link CountQuery} object for efficiency reasons.
     */
  public count(): number {
    return this.buildCount().count();
  }
}
